<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css" type="text/css"/> 
    <title>Document</title>
</head>
<body class="body">
<header class="header">
    <a href="#">
        <img class="img" src="https://dpo.mospolytech.ru/content/themes/default/img/logo_black.png" alt="logo">
    </a>
    <p class="text-header">
        <?php echo 
        'Работа №2'; 
        ?>
    </p>
</header>
<main class="text-main">
    <!-- Форма регистрации -->
    <form action="https://httpbin.org/post" method="POST">

        <label for="name_id"> Name: </label>
        <input type="text" name="name" id="name_id">

        <label for="email_id"> Email: </label>
        <input type="email" name="email" id="email_id" class="email">

        <label class="forms-text"> Type massage: </label>
        <fieldset class="forms-information__checkbox checkbox"> 
            <label class="checkbox-item">
                <input class="forms-checkbox" type="checkbox" name="grade" value="cool">
                <span class="checkbox-text"> Cool </span>
            </label>
            <label class="checkbox-item">
                <input class="forms-checkbox" type="checkbox" name="grade" value="badly">
                <span class="checkbox-text"> Bad </span>
            </label>
            <label class="checkbox-item">
                <input class="forms-checkbox" type="checkbox" name="grade" value="sentence">
                <span class="checkbox-text"> Sentence </span>
            </label>
        </fieldset>

        <label for="appeal_id"> Text massage </label>
        <input type="text" name="appeal" id="appeal_id">

        <label class="forms-text"> Answer options: </label>
        <fieldset class="forms-information__checkbox checkbox"> 
            <label class="checkbox-item">
                <input class="forms-checkbox" type="checkbox" name="options" value="sms">
                <span class="checkbox-text"> Sms </span>
            </label>
            <label class="checkbox-item">
                <input class="forms-checkbox" type="checkbox" name="options" value="email">
                <span class="checkbox-text"> Email </span>
            </label>
        </fieldset>

        <button type="submite"> Send </button>
    </form> 
    <a href="result.php" class=""> Second page </a>
</main>
<footer>
    <p class="text-footer">
        <?php echo 
        'Задание: Собрать сайт из двух страниц. 1 страница: Сверстать форму обратной связи. 
        Отправку формы осуществить на URL: https://httpbin.org/post. Добавить кнопку для перехода на 2 
        страницу. 2 страница: вывести на страницу результат работы функции get_headers. Загрузить код в 
        удаленный репозиторий. Залить на хостинг.'; 
        ?>
    </p>  
</footer>
</body>