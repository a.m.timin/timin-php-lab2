<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css" type="text/css"/> 
    <title>Document</title>
</head>
<body class="body">
<header class="header">
    <a href="#">
        <img class="img" src="https://dpo.mospolytech.ru/content/themes/default/img/logo_black.png" alt="logo">
    </a>
    <p class="text-header">
        <?php echo 
        'Работа №2'; 
        ?>
    </p>
</header>
<main>
    <?php
        $url = "https://httpbin.org";
        $answer = get_headers($url);
    ?>
    <textarea name="" id="" cols="50" rows="20">
        <?php
            print_r($answer);
        ?>
    </textarea>
</main>
<footer>
    <p class="text-footer">
        <?php echo 
        'Задание: Собрать сайт из двух страниц. 1 страница: Сверстать форму обратной связи. 
        Отправку формы осуществить на URL: https://httpbin.org/post. Добавить кнопку для перехода на 2 
        страницу. 2 страница: вывести на страницу результат работы функции get_headers. Загрузить код в 
        удаленный репозиторий. Залить на хостинг.'; 
        ?>
    </p>  
</footer>
</body>
</html>